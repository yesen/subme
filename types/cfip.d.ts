type Cfip = {
  name: string;
  code: string;
  ip: string;
  id?: string;
};
