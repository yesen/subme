type ButtonType = "submit" | "reset" | "button";
type ButtonSize = "normal" | "sm" | "lg";
type ButtonColor = "default" | "primary" | "info" | "danger";
