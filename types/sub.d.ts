type Sub = { id: string };

type Subscribe = {
  name: string;
  type: SubmeNodeType;
  server: string;
  port: number;
  cipher?: "auto";
  password?: string;
  uuid?: string;
  alterId?: 0;
  tls?: true;
  servername?: string;
  network: "ws";
  "ws-opts": {
    path: string;
    headers: {
      Host: string;
    };
  };
  sni?: string;
};

type SubscribeImport = {
  nodes: Subscribe[];
  clear: boolean;
};
