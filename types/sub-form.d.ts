type SubForm = {
  id: string;
  type: "v2ray" | "clash";
  cfip: string;
  skip: string;
  url: string;
  includeOrgin: boolean;
};
