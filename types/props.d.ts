type ChildrenProps = {
  children: React.ReactNode;
};

type ComponentProps = {
  className?: string;
} & ChildrenProps;
