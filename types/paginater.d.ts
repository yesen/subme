type Paginater<T> = {
  total: number;
  total_page: number;
  page: number;
  page_size: number;
  data: T[];
};
