type SubmeNodeType = "vmess" | "trojan";

type SubmeNode = {
  type: SubmeNodeType;
  name: string;
  host: string;
  port: number;
  password: string;
  path: string;
  id?: string;
};
