"use client";

import Button from "@/components/Button";

import React, { useState } from "react";

export default function NodeIndexUI({
  p,
  keyword = "",
}: {
  p: Paginater<SubmeNode>;
  keyword?: string;
}) {
  const [kw, setKw] = useState(keyword);
  return (
    <>
      <form
        className="flex justify-start items-center space-x-2 my-3"
        autoComplete="off"
      >
        <label>
          <input
            name="keyword"
            value={kw}
            onChange={(e) => setKw(e.target.value.trim())}
            placeholder="关键字"
          />
        </label>
        <Button href={`/node?keyword=${kw}`} color="primary">
          搜索
        </Button>
      </form>
      <div className="prose max-w-none">
        <table>
          <thead>
            <tr>
              <th>#</th>
              <th>名称</th>
              <th>类型</th>
              <th>地址</th>
              <th>操作</th>
            </tr>
          </thead>
          <tbody>
            {p.data.map((n) => (
              <tr key={n.id!} className="odd:bg-gray-100">
                <td>{n.id!}</td>
                <td>{n.name}</td>
                <td>{n.type}</td>
                <td>
                  {n.host}:{n.port}
                </td>
                <td>
                  <div className="flex justify-start items-start gap-x-2">
                    <Button
                      href={`/node/${n.id!}/edit?_r=${Math.random()}`}
                      size="sm"
                      color="primary"
                    >
                      修改
                    </Button>
                    <Button
                      href={`/node/${n.id!}/clone?_r=${Math.random()}`}
                      size="sm"
                      color="info"
                      onClick={(e) => {
                        if (!window.confirm(`确定克隆节点【${n.name}】吗？`)) {
                          e.preventDefault();
                        }
                      }}
                    >
                      克隆
                    </Button>
                    <Button
                      size="sm"
                      color="danger"
                      href={`/node/${n.id!}/del?_r=${Math.random()}`}
                      onClick={(e) => {
                        if (!window.confirm(`确定删除节点【${n.name}】吗？`)) {
                          e.preventDefault();
                        }
                      }}
                    >
                      删除
                    </Button>
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </>
  );
}
