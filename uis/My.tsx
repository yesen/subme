"use client";

import { Form, Input } from "@/components/Form";
import React from "react";
import { newId } from "@/utils/id";

export default function MyUI() {
  return (
    <>
      <div>{newId()}</div>
      <Form>
        <Input label="Email" required />
        <Input label="现用密码" required />
        <Input label="新密码" required />
        <Input label="重复新密码" required />
      </Form>
    </>
  );
}
