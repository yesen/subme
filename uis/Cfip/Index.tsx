"use client";

import Button from "@/components/Button";
import React from "react";

export default function CfipIndexUI({ cfips }: { cfips: Cfip[] }) {
  return (
    <>
      <div className="prose max-w-none">
        <table>
          <thead>
            <tr>
              <th>#</th>
              <th>名称</th>
              <th>代码</th>
              <th>IP</th>
              <th>操作</th>
            </tr>
          </thead>
          <tbody>
            {cfips.map((cf) => (
              <tr key={cf.id!} className="odd:bg-gray-100">
                <td>{cf.id!}</td>
                <td>{cf.name}</td>
                <td>{cf.code}</td>
                <td>{cf.ip}</td>
                <td>
                  <div className="flex justify-start items-start gap-x-2">
                    <Button
                      href={`/cfip/${cf.id!}/edit`}
                      size="sm"
                      color="primary"
                    >
                      修改
                    </Button>
                    <Button
                      size="sm"
                      color="danger"
                      href={`/cfip/${cf.id!}/del`}
                      onClick={(e) => {
                        if (!window.confirm("确定删除？")) {
                          e.preventDefault();
                        }
                      }}
                    >
                      删除
                    </Button>
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </>
  );
}
