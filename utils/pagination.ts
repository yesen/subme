export const DEFAULT_PAGE_SIZE: number = 30;

export const newPagination = <T>(total: number, page: number, data: T[]) => {
  const page_size: number = DEFAULT_PAGE_SIZE;
  const total_page = parseInt(Math.ceil(total / page_size).toFixed(0), 10);
  const p: Paginater<T> = {
    total,
    total_page,
    page,
    page_size,
    data,
  };
  return p;
};
