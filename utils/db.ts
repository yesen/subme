import sqlite3 from "sqlite3";
import { open, Database } from "sqlite";
import path from "node:path";

let _db: Database<sqlite3.Database, sqlite3.Statement> | null = null;

/// 获取数据库连接
export const getDB = async () => {
  if (!_db) {
    const filename = path.resolve("./subme.db");
    _db = await open({ filename, driver: sqlite3.Database });
  }
  return _db;
};

export const closeDB = async () => {
  await _db?.close();
  _db = null;
};
