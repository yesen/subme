import xid from "xid-js";

export const newId = () => xid.next() as string;
