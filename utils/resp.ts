export const newResp = <T>(code: number, msg: string, data: T) => {
  const resp: JsonResp<T> = {
    code,
    msg,
    data,
  };
  return new Response(JSON.stringify(resp), {
    headers: { "Content-Type": "application/json" },
  });
};

export const ok = <T>(data: T) => newResp<T>(0, "OK", data);

export const failed_with_code = (code: number, msg: string) =>
  newResp<{}>(code, msg, {});

export const err_with_code = (code: number, e: Error) =>
  failed_with_code(code, e.message);

export const failed = (msg: string) => failed_with_code(-1, msg);

export const err = (e: Error) => err_with_code(-1, e);
