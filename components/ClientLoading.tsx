"use client";

import React from "react";
import Loading from "./Loading";

export default function ClientLoading() {
  return (
    <div className="fixed inset-0 bg-gray-950/80 z-50">
      <Loading />
    </div>
  );
}
