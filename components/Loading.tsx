import React from "react";

export default function Loading() {
  return (
    <div className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 p-6 bg-orange-600 text-white border rounded-md shadow-md text-lg">
      正在处理，请稍等……
    </div>
  );
}
