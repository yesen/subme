import React from "react";

export default function Alter({ children, className = "" }: ComponentProps) {
  return (
    <div
      className={`bg-cyan-500 text-white px-6 py-4 border rounded my-3 ${className}`}
    >
      {children}
    </div>
  );
}
