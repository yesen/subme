"use client";

import Link from "next/link";
import React from "react";

type Menu = { key: string; label: string; children?: Menu[] };

const menus: Menu[] = [
  {
    key: "node",
    label: "节点管理",
    children: [
      { key: "/node", label: "节点列表" },
      { key: "/node/add", label: "添加节点" },
      { key: "/node/import", label: "导入节点" },
    ],
  },
  {
    key: "cfip",
    label: "优选管理",
    children: [
      { key: "/cfip", label: "优选列表" },
      { key: "/cfip/add", label: "添加优选" },
    ],
  },
  {
    key: "sub",
    label: "订阅管理",
    children: [{ key: "/sub", label: "订阅管理" }],
  },
  // {
  //   key: "my",
  //   label: "账号管理",
  //   children: [
  //     // { key: "/my", label: "修改密码" },
  //     { key: "/logout", label: "退出登录" },
  //   ],
  // },
];

export default function Layout({ children }: ChildrenProps) {
  return (
    <>
      <aside className="bg-blue-100 w-72 fixed h-screen">
        <h2 className="font-normal text-3xl py-6 text-center">Subme!</h2>
        <ul className="p-3 flex flex-col gap-y-3">
          {menus.map((m) => (
            <li
              key={m.key}
              className="p-3 border rounded bg-blue-50 flex flex-col gap-y-2"
            >
              <h3 className="text-lg">{m.label}</h3>

              <ul className="px-2 flex flex-col gap-y-2">
                {m.children!.map((cm) => (
                  <li key={cm.key}>
                    <Link
                      href={`${cm.key}?_r=${Math.random()}`}
                      className="hover:text-red-600"
                    >
                      {cm.label}
                    </Link>
                  </li>
                ))}
              </ul>
            </li>
          ))}
        </ul>
      </aside>
      <div className="relative ml-72 p-6 ">{children}</div>
    </>
  );
}
