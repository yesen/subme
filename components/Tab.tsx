"use client";

import React, { useState } from "react";

export default function Tab({
  items,
  className = "",
  value = "",
  onChange = () => {},
}: {
  className?: string;
  items: { value: string; label: string }[];
  onChange: (value: string) => void;
  value?: string;
}) {
  const [currentValue, setCurrentValue] = useState(value || "");
  const onChangeHandler = (v: string) => {
    setCurrentValue(v);
    onChange(v);
  };

  const itemCss = "";

  return (
    <ul
      className={`border-b flex justify-start items-center  pl-6 space-x-3 ${className}`}
    >
      {items.map((i) => (
        <li
          className={`px-3 py-1 border border-b-0 cursor-pointer rounded-tl rounded-tr ${
            currentValue === i.value ? "bg-cyan-600 text-white" : ""
          }`}
          onClick={() => {
            onChangeHandler(i.value);
          }}
          key={i.value}
        >
          {i.label}
        </li>
      ))}
    </ul>
  );
}
