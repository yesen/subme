import React from "react";

export default function PageTitle({
  children,
  className = "",
}: ComponentProps) {
  return (
    <h2 className={`text-xl my-3 font-semibold ${className}`}>{children}</h2>
  );
}
