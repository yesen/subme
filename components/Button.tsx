import Link from "next/link";
import React from "react";

function Button({
  children,
  className = "",
  type = "button",
  size = "normal",
  color = "default",
  href = undefined,
  onClick = undefined,
}: ComponentProps & {
  type?: ButtonType;
  size?: ButtonSize;
  color?: ButtonColor;
  href?: string;
  onClick?: React.MouseEventHandler<HTMLButtonElement | HTMLAnchorElement>;
}) {
  let sizeCss = "px-3 py-1";
  switch (size) {
    case "lg":
      sizeCss = "px-5 py-3 text-lg";
      break;
    case "sm":
      sizeCss = "px-2 py-1 text-sm";
      break;
  }
  let colorCss = "bg-gray-100 border-gray-300 hover:bg-gray-200";
  switch (color) {
    case "primary":
      colorCss = "bg-blue-600 border-blue-700 text-white hover:bg-blue-700";
      break;
    case "info":
      colorCss = "bg-cyan-600 border-cyan-700 text-white hover:bg-cyan-700";
      break;
    case "danger":
      colorCss = "bg-red-600 border-red-700 text-white hover:bg-red-700";
      break;
  }
  if (href) {
    return (
      <Link
        href={href}
        className={`no-underline border rounded ${sizeCss} ${colorCss} ${className}`}
        onClick={onClick}
      >
        {children}
      </Link>
    );
  }
  return (
    <button
      className={`border rounded ${sizeCss} ${colorCss} ${className}`}
      type={type}
      onClick={onClick}
    >
      {children}
    </button>
  );
}

export default Button;
