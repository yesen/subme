import React from "react";

export default function Element({
  children,
  label,
  className = "",
}: FormElementProps) {
  return (
    <label className={`block ${className}`}>
      <span className="text-gray-700">{label}</span>
      {children}
    </label>
  );
}
