import React from "react";
import Element from "./Element";

export default function TextArea({
  label,
  value = "",
  className = "",
  placeholder = undefined,
  required = false,
  onChange = undefined,
  rows = undefined,
}: {
  label: string;
  className?: string;
  placeholder?: string;
  required?: boolean;
  onChange?: React.ChangeEventHandler<HTMLTextAreaElement>;
  value?: string | ReadonlyArray<string> | number;
  rows?: number;
}) {
  return (
    <Element label={label}>
      <textarea
        className={`mt-1 block w-full ${className}`}
        placeholder={placeholder}
        required={required}
        onChange={onChange}
        rows={rows}
        value={value}
      />
    </Element>
  );
}
