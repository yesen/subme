import React from "react";
import Element from "./Element";

export default function Label({
  label,
  children,
  className = "",
  onClick,
  onDoubleClick,
}: {
  label: string;
  onClick?: React.MouseEventHandler<HTMLSpanElement>;
  onDoubleClick?: React.MouseEventHandler<HTMLSpanElement>;
} & ComponentProps) {
  return (
    <Element label={label}>
      <span
        className={`mt-1 block w-full form-input ${className}`}
        onClick={onClick}
        onDoubleClick={onDoubleClick}
      >
        {children}
      </span>
    </Element>
  );
}
