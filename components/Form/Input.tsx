import React from "react";
import Element from "./Element";

export default function Input({
  label,
  type = "text",
  className = "",
  placeholder = undefined,
  required = false,
  onChange = undefined,
  value = undefined,
  readonly,
}: {
  label: string;
  type?: React.HTMLInputTypeAttribute;
  className?: string;
  placeholder?: string;
  required?: boolean;
  onChange?: React.ChangeEventHandler<HTMLInputElement>;
  value?: string | ReadonlyArray<string> | number;
  readonly?: boolean;
}) {
  return (
    <Element label={label}>
      <input
        type={type}
        className={`mt-1 block w-full ${className}`}
        placeholder={placeholder}
        required={required}
        onChange={onChange}
        value={value}
        readOnly={readonly}
      />
    </Element>
  );
}
