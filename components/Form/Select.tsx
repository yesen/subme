import React from "react";
import Element from "./Element";

export default function Select({
  label,
  className = "",
  placeholder = undefined,
  required = false,
  onChange = undefined,
  value = undefined,
  items = undefined,
}: {
  label: string;
  className?: string;
  placeholder?: string;
  required?: boolean;
  onChange?: React.ChangeEventHandler<HTMLSelectElement>;
  value?: string | ReadonlyArray<string> | number;
  items?: { value: string | ReadonlyArray<string> | number; label: string }[];
}) {
  return (
    <Element label={label}>
      <select
        className={`mt-1 block w-full ${className}`}
        placeholder={placeholder}
        required={required}
        onChange={onChange}
        value={value}
      >
        {items &&
          items.map((item) => (
            <option value={item.value} key={item.value.toString()}>
              {item.label}
            </option>
          ))}
      </select>
    </Element>
  );
}
