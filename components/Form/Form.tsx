"use client";

import React from "react";
import Button from "../Button";

export default function Form({
  children,
  className = "",
  includeCancel = true,
  includeOk = true,
  okCallback = undefined,
  cancelCallback = () => {
    history.back();
  },
  okLabel = "确定",
  cancelLabel = "取消",
  onSubmit = undefined,
}: ComponentProps & {
  onSubmit?: React.FormEventHandler<HTMLFormElement>;
  includeCancel?: boolean;
  includeOk?: boolean;
  okCallback?: React.MouseEventHandler<HTMLButtonElement | HTMLAnchorElement>;
  cancelCallback?: React.MouseEventHandler<
    HTMLButtonElement | HTMLAnchorElement
  >;
  okLabel?: string;
  cancelLabel?: string;
}) {
  return (
    <form
      autoComplete="off"
      className={`grid grid-cols-1 gap-6 ${className}`}
      onSubmit={(e) => {
        e.preventDefault();
        if (onSubmit) {
          onSubmit(e);
        }
      }}
    >
      {children}

      <div className="my-2 flex justify-start items-center gap-x-2">
        {includeOk && (
          <Button color="primary" type="submit" onClick={okCallback}>
            {okLabel}
          </Button>
        )}
        {includeCancel && (
          <Button color="default" type="button" onClick={cancelCallback}>
            {cancelLabel}
          </Button>
        )}
      </div>
    </form>
  );
}
