import React from "react";
import "./globals.css";
import type { Metadata } from "next";
import Layout from "@/components/Layout";

export const metadata: Metadata = {
  title: "Subme!",
  description: "订阅管理",
};

export default function RootLayout({ children }: ChildrenProps) {
  return (
    <html lang="zh-Hans">
      <body>
        <Layout>{children}</Layout>
      </body>
    </html>
  );
}
