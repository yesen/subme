import PageTitle from "@/components/PageTitle";
import CfipInputUI from "@/uis/Cfip/Input";
import React from "react";

export default function CfipAddPage() {
  return (
    <>
      <PageTitle>添加优选</PageTitle>
      <CfipInputUI />
    </>
  );
}
