import { DELETE } from "@/utils/fetch";
import { redirect } from "next/navigation";

export default async function CfipDelPage({
  params: { id },
}: {
  params: { id: string };
}) {
  await DELETE(`/cfip/${id}`);
  return redirect(`/cfip?msg=删除成功&_r=${Math.random()}`);
}
