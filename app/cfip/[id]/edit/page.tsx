import PageTitle from "@/components/PageTitle";
import CfipInputUI from "@/uis/Cfip/Input";
import { GET } from "@/utils/fetch";
import React from "react";

export default async function CfipEditPage({
  params: { id },
}: {
  params: { id: string };
}) {
  const resp: JsonResp<Cfip> = await GET(`/cfip/${id}`);
  return (
    <>
      <PageTitle>修改优选</PageTitle>
      <CfipInputUI data={resp.data} />
    </>
  );
}
