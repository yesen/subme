import Alter from "@/components/Alter";
import PageTitle from "@/components/PageTitle";
import CfipIndexUI from "@/uis/Cfip/Index";
import { GET } from "@/utils/fetch";
import React from "react";

export default async function CfipIndexPage({
  searchParams: { msg },
}: {
  searchParams: { msg?: string };
}) {
  const resp: JsonResp<Cfip[]> = await GET("/cfip");
  return (
    <>
      <PageTitle>优选列表</PageTitle>

      {msg && <Alter>{msg}</Alter>}

      <CfipIndexUI cfips={resp.data} />
    </>
  );
}
