import { closeDB, getDB } from "@/utils/db";
import { err, ok } from "@/utils/resp";

export async function GET(
  _: Request,
  { params: { id } }: { params: { id: string } }
) {
  try {
    const db = await getDB();
    const cfip: Cfip | undefined = await db.get(
      "SELECT id,name,code,ip FROM cfips WHERE id = ?",
      id
    );
    await closeDB();
    if (!cfip) {
      throw new Error("不存在的记录");
    }
    return ok(cfip!);
  } catch (e) {
    return err(e as Error);
  }
}

export async function DELETE(
  _: Request,
  { params: { id } }: { params: { id: string } }
) {
  try {
    const db = await getDB();
    const r = await db.run("DELETE FROM cfips WHERE id = ?", id);
    await closeDB();

    return ok({ id, aff: r.changes });
  } catch (e) {
    return err(e as Error);
  }
}
