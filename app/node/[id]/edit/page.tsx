import PageTitle from "@/components/PageTitle";
import NodeInputUI from "@/uis/Node/Input";
import { GET } from "@/utils/fetch";
import React from "react";

export default async function NodeEditPage({
  params: { id },
}: {
  params: { id: string };
}) {
  const resp: JsonResp<SubmeNode> = await GET(`/node/${id}`);
  return (
    <>
      <PageTitle>修改节点</PageTitle>
      <NodeInputUI data={resp.data} />
    </>
  );
}
