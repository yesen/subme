import { DELETE } from "@/utils/fetch";
import { redirect } from "next/navigation";

export default async function NodeDelPage({
  params: { id },
}: {
  params: { id: string };
}) {
  await DELETE(`/node/${id}`);
  return redirect(`/node?msg=删除成功&_r=${Math.random()}`);
}
