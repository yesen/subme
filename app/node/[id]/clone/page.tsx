import { POST } from "@/utils/fetch";
import { redirect } from "next/navigation";
import React from "react";

export default async function NodeClonePage({
  params: { id },
}: {
  params: { id: string };
}) {
  const resp: JsonResp<SubmeNode> = await POST(`/node/${id}`, {});
  return redirect(`/node/${resp.data.id!}/edit?_r=${Math.random()}`);
}
