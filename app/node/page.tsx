import Alter from "@/components/Alter";
import PageTitle from "@/components/PageTitle";
import NodeIndexUI from "@/uis/Node/Index";
import { GET } from "@/utils/fetch";
import Link from "next/link";
import React from "react";

export default async function NodeIndexPage({
  searchParams: { msg, page = 0, keyword = "" },
}: {
  searchParams: { msg?: string; page?: number; keyword?: string };
}) {
  const resp: JsonResp<Paginater<SubmeNode>> = await GET(
    `/node?page=${page}&keyword=${keyword}`
  );
  const pageNums: number[] = [];
  for (let i = 0; i < resp.data.total_page; i++) {
    pageNums.push(i);
  }
  return (
    <>
      <PageTitle>节点列表</PageTitle>
      {msg && <Alter>{msg}</Alter>}
      <NodeIndexUI p={resp.data} />
      <ul className="my-3 flex justify-center items-center gap-x-1">
        {pageNums.map((n) => (
          <li key={n}>
            <Link
              href={`/node?page=${n}&keyword=${keyword}&_r=${Math.random()}`}
              className={`block px-3 py-1 border  ${
                n == page
                  ? "text-white bg-blue-600 hover:bg-blue-700"
                  : "hover:bg-gray-100"
              }`}
            >
              {n + 1}
            </Link>
          </li>
        ))}
      </ul>
    </>
  );
}
