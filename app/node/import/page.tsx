import PageTitle from "@/components/PageTitle";
import NodeImportUI from "@/uis/Node/Import";
import React from "react";

export default function NodeImportPage() {
  return (
    <>
      <PageTitle>导入节点</PageTitle>
      <NodeImportUI />
    </>
  );
}
