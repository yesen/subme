import PageTitle from "@/components/PageTitle";
import NodeInputUI from "@/uis/Node/Input";
import React from "react";

export default function NodeAddPage() {
  return (
    <>
      <PageTitle>添加节点</PageTitle>
      <NodeInputUI />
    </>
  );
}
