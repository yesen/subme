import Loading from "@/components/Loading";
import React from "react";

export default function LoadingPage() {
  return (
    <div className="fixed inset-0 z-50">
      <Loading />
    </div>
  );
}
