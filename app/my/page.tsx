import PageTitle from "@/components/PageTitle";
import MyUI from "@/uis/My";
import React from "react";

export default function MyPage() {
  return (
    <>
      <PageTitle>修改密码</PageTitle>
      <MyUI />
    </>
  );
}
