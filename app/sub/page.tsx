import PageTitle from "@/components/PageTitle";
import SubUI from "@/uis/Sub";
import React from "react";

export default async function SubPage() {
  return (
    <>
      <PageTitle>订阅</PageTitle>

      <SubUI />
    </>
  );
}
